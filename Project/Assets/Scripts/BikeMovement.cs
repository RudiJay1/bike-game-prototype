﻿using UnityEngine;
using System.Collections;

public class BikeMovement : MonoBehaviour {

    [SerializeField]
    private Rigidbody m_bikeBody, m_bikeFront, m_frontWheel, m_backWheel;
    [SerializeField]
    private GameObject m_bikeSize, m_camera;

    [SerializeField]
    private GameObject m_bottle;

    [SerializeField]
    private GameObject m_getUpText;

    [SerializeField]
    private float m_turnSpeed, m_pedalSpeed, m_jumpForce, m_distToGround, m_distToSide;
    private float m_turnAmount, m_pedalAmount;

    void Awake ()
    {
        //set higher angular velocity than default
        m_frontWheel.maxAngularVelocity = 15;
        m_backWheel.maxAngularVelocity = 15;
        m_bikeFront.maxAngularVelocity = 15;
    }

    void Start()
    {
        //get the height from the ground
        m_distToGround = (m_bikeSize.transform.localScale.y / 2) + 0.1f;
        m_distToSide = (m_bikeSize.transform.localScale.z / 2) + 0.1f;
    }

	void Update ()
    {
        Debug.DrawRay(m_bikeSize.transform.position, -m_bikeSize.transform.up);

        if (IsFallen())
        {
            m_getUpText.SetActive(true);
        }
        else
        {
            m_getUpText.SetActive(false);
        }

        //rotate handles and front wheel of bike using horizontal input axis
        m_bikeFront.transform.Rotate(new Vector3(0, 0, Input.GetAxis("Horizontal") * m_turnSpeed));
        //turnAmount = Input.GetAxis("Horizontal") * turnSpeed;
        
        //pedal
        m_pedalAmount = Input.GetAxis("Vertical") * m_pedalSpeed;
        
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            Jump();
        }

        if (Input.GetKeyDown(KeyCode.R) && IsFallen())
        {
            GetUp();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            ThrowBottle();
        }
	}

    void FixedUpdate()
    {
        //turns handles
        //bikeFront.AddTorque(bikeFront.transform.forward * turnAmount);

        //moves bike forwards if upright
        if (IsGrounded())
        {
            m_frontWheel.AddTorque(-m_frontWheel.transform.up * m_pedalAmount);
            m_backWheel.AddTorque(-m_backWheel.transform.up * m_pedalAmount);
        }

    }

    private bool IsGrounded()
    {
        return Physics.Raycast(m_bikeSize.transform.position, -m_bikeSize.transform.up, m_distToGround);
    }

    private bool IsFallen()
    {
        bool fallen = false;

        //if the bike isn't standing upright, and is also against the ground;
        if (!IsGrounded() && (Physics.Raycast(m_bikeSize.transform.position, m_bikeSize.transform.forward, m_distToSide)
             || Physics.Raycast(m_bikeSize.transform.position, -m_bikeSize.transform.forward, m_distToSide)))
        {
            fallen = true;
        }

        return fallen;
    }

    private void Jump()
    {
        m_bikeBody.AddForce(m_bikeBody.transform.up * m_jumpForce);
    }

    private void GetUp()
    {
        m_bikeBody.transform.position += new Vector3(0, 1, 0);

        m_bikeBody.transform.rotation = Quaternion.Euler(0,
            m_bikeBody.transform.rotation.eulerAngles.y, m_bikeBody.transform.rotation.eulerAngles.z);
    }

    private void ThrowBottle()
    {
        //GameObject bottle = Instantiate(m_bottle, m_camera.transform.position, m_camera.transform.rotation);
    }
}

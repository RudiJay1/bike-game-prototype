# README #

Thank you for using my Bike Game Prototype repository. Included is a small scale Unity3D prototype for a heavily physics based first-person bike game.

### How do I get set up? ###

* Clone the repository
* Open Unity (Version 5.4.0 recommended)
* Open the "Project" folder in the repository folder in Unity
* Open scene Test.unity
* Press the play button to begin

### Controls ###
* W/UpArrow - Accelerate
* S/DownArrow - Decelerate
* AD/LeftArrowRightArrow - Steer
* Spacebar - Jump
* R - Reset position

### Credits ###

Code and scene - Rudi Prentice (http://www.rudiprentice.com)
Bike model - Downloaded from Archibase (http://archibaseplanet.com/download/5dd3e597.html)